VERSION=$(shell poetry version -s)
PACKAGE_NAME=$(shell ls src)


ifeq ($(OS),Windows_NT)
RM = del /Q /F
RRM = rmdir /Q /S
WHICH = where.exe
else
RM = rm -f
RRM = rm -f -r
WHICH = which
endif

.PHONY: help reformat check tests clean tox docs poetry flake8 mypy isort prepare build 

help:
	@echo -e "$(PACKAGE_NAME) version $(VERSION) -- Makefile"
	@echo -e ""
	@echo -e "\t help\t\t cf. \`make help\`"
	@echo -e "\t flake8\t\t Check code style"
	@echo -e "\t mypy\t\t Check static typing"
	@echo -e "\t isort\t\t Check import order"
	@echo -e "\t tests\t\t Run unit tests"
	@echo -e "\t tox\t\t Run unit tests for different python versions"
	@echo -e "\t build\t\t Build as a wheel"
	@echo -e ""
	@echo -e "\t clean:\t\t Nettoyage complet du dossier de construction"
	@echo -e "\t reformat \t Reformate le code"

clean:
	$(RRM) dist .mypy_cache .pytest_cache htmlcov test_report || echo ""
	$(RRM) docs/build || echo ""
	$(RM) report.xml .coverage coverage.xml || echo ""

poetry:
ifeq ($(DOCKER_ENV), true)
	$(WHICH) poetry || pip install poetry --break-system-packages
else
	$(WHICH) poetry || pip install poetry
endif
	poetry install

flake8: poetry
	poetry run flake8 --ignore=E501,W503,F405,F403,E226,E251,E202,E251,E225 tests
	poetry run flake8 --ignore=E501,W503,F405,F403,E226,E251,E202,E251,E225 src

mypy: poetry
	poetry run mypy --ignore-missing  --check-untyped-defs src
	poetry run mypy --ignore-missing  --check-untyped-defs tests

isort: poetry
	poetry run isort --check src
	poetry run isort --check tests

reformat: poetry
	poetry run black .
	poetry run isort .
	poetry run autoflake --in-place --remove-all-unused-imports --recursive . || true

prepare: reformat isort flake8 mypy tests

build: poetry
	poetry build

tox: poetry
	poetry run tox

tests: poetry
	poetry install
	poetry run pytest -vvv tests/ --cov=src --cov-report=html --junitxml=report.xml --html-report=./test_report --title='$(PACKAGE_NAME) test report'
	poetry run coverage xml
	poetry run coverage report

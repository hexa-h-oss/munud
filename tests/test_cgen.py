import pkgutil

import yaml

from munud.cgen import build_code_from_payload_format
from munud.cgen_types import HEADER


def test_c_gen():
    fmt_file = pkgutil.get_data(__name__, "assets/test_payload.yml")
    c_expected_result_file = pkgutil.get_data(__name__, "assets/test_cgen.h")

    assert fmt_file is not None
    assert c_expected_result_file is not None

    fmt = yaml.safe_load(fmt_file.decode())

    c_file_result = build_code_from_payload_format(
        fmt,
        "\n",
    )

    c_expected_result = c_expected_result_file.decode().replace("\r\n", "\n")

    c_file_result = c_file_result.replace("\r\n", "\n")

    header_len = len(HEADER)

    # yank header
    assert c_file_result[header_len:] == c_expected_result[header_len:]

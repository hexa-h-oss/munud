from click.testing import CliRunner

from munud.main import main


def test_show_nofmt():
    runner = CliRunner()
    result = runner.invoke(main, ["show"])
    assert result.exit_code == 0
    assert "Please specify a format" in result.output


def test_cgen_nofmt():
    runner = CliRunner()
    result = runner.invoke(main, ["cgen"])
    assert result.exit_code == 0
    assert "Please specify a format" in result.output


def test_show_fmt():
    runner = CliRunner()
    result = runner.invoke(main, ["--fmt", "tests/assets/test_payload.yml", "show"])
    assert result.exit_code == 0


def test_cgen_fmt():
    runner = CliRunner()
    result = runner.invoke(main, ["--fmt", "tests/assets/test_payload.yml", "cgen"])
    assert result.exit_code == 0


def test_decode_fmt():
    runner = CliRunner()
    result = runner.invoke(
        main,
        [
            "--fmt",
            "tests/assets/test_payload.yml",
            "decode",
            "--payload",
            "AE25AEFD2156A325",
        ],
    )
    assert result.exit_code == 0

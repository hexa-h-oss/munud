from munud.munud_types import UnalignedUInt


def test_multiple_bytes_aligned_right():

    a = UnalignedUInt(19, 3 * 8 + 5, True)

    toto = [0] * 12
    a.shove(toto, 0b1111111111111111111)
    print(toto)

from munud.munud_types import UnalignedUInt

byte_buffer = b"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"


def test_single_byte_unaligned():
    a = UnalignedUInt(3 * 8 + 3, 2)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))

    print("\n")


def test_single_byte_aligned():

    a = UnalignedUInt(3 * 8, 8)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))
    print("\n")


def test_single_byte_aligned_right():

    a = UnalignedUInt(3 * 8 + 3, 5)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))

    print("\n")


def test_single_byte_aligned_left():

    a = UnalignedUInt(3 * 8, 4)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))
    print("\n")


def test_multiple_bytes_aligned():

    a = UnalignedUInt(3 * 8, 16, True)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))

    print("\n")


def test_multiple_bytes_unaligned():
    a = UnalignedUInt(3 * 8 + 5, 15, True)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))

    print("\n")


def test_multiple_bytes_aligned_left():
    a = UnalignedUInt(3 * 8, 15, True)

    print(a.op_read())
    for op in a.op_write():
        print(op)

    print(a.extract(byte_buffer))
    print("\n")


def test_multiple_bytes_aligned_right():

    a = UnalignedUInt(3 * 8 + 5, 19, True)

    print(a.op_read())
    for op in a.op_write():
        print(op)
    print(a.extract(byte_buffer))

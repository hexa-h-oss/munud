import textwrap

from munud import MunudDecoder


def test_from_yml_decode():
    expected = {
        "version": 3,
        "mode": 620,
        "pressure": 281,
        "vbat": 77,
        "event_datetime": 759999692,
    }

    assert (
        MunudDecoder.from_yaml_file("./tests/assets/test_payload.yml").decode(
            b"sdfijef`"
        )
        == expected
    )


def test_from_yml_encode():
    expected = {
        "version": 3,
        "mode": 620,
        "pressure": 281,
        "vbat": 77,
        "event_datetime": 759999692,
    }

    assert (
        MunudDecoder.from_yaml_file("./tests/assets/test_payload.yml").encode(expected)
        == b"sdfijef`"
    )


def test_encode_decode():
    expected = {
        "version": 3,
        "mode": 620,
        "pressure": 281,
        "vbat": 77,
        "event_datetime": 759999692,
    }

    decoder = MunudDecoder.from_yaml_file("./tests/assets/test_payload.yml")

    assert decoder.decode(decoder.encode(expected)) == expected


def test_encode_decode_from_str():
    expected = {
        "version": 3,
        "mode": 620,
        "pressure": 281,
        "vbat": 77,
        "event_datetime": 759999692,
    }

    decoder = MunudDecoder.from_yaml(
        textwrap.dedent(
            """
            - name: version
              size: 3
              type: uint8_t
            - name: mode
              size: 10
              type: uint16_t
            - name: pressure
              size: 9
              type: uint8_t
            - name: vbat
              size: 7
              type: uint8_t
            - name: event_datetime
              size: 32
              type: uint32_t
        """
        ),
    )

    assert decoder.decode(decoder.encode(expected)) == expected

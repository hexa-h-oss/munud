
/*
* This file was automatically generated using munud 0.2.0.
*
* This file should not be edited directly, any changes will be
* overwritten next time the script is run.
*
* Generated on 09/27/2024 at 17:45:01.
*
* Source code for munud is available at:
* https://gitlab.com/hexa-h/munud
*/

#include <assert.h>
#include <stdint.h>
        
#ifndef DEFAULT_MUNUD_HEADER_GUARD_H
#define DEFAULT_MUNUD_HEADER_GUARD_H


#define MUNUD_PAYLOAD_SIZE 61



inline static uint8_t get_version(uint8_t *payload)
{
    /*
     * This function retrives the value version (3 bits).
     * The value is found in byte 0.
     * The value starts at bit 0.
     */

    return payload[0] >> 5;
}

inline static void set_version(uint8_t *payload, uint8_t value)
{
    /*
     * This function writes the value version (3 bits).
     * The value will be written in byte 0.
     * The value starts at bit 0.
     */

    payload[0] |= value << 5;
}


inline static uint16_t get_mode(uint8_t *payload)
{
    /*
     * This function retrives the value mode (10 bits).
     * The value spreads between bytes 0 and 1.
     * The value starts at bit 3.
     */

    return (payload[0] & 0x1F) << 5 | payload[1] >> 3;
}

inline static void set_mode(uint8_t *payload, uint16_t value)
{
    /*
     * This function writes the value mode (10 bits).
     * The value will spread between bytes 0 and 1.
     * The value starts at bit 3.
     */

    payload[0] |= (value >> 5) & 0x1F;
    payload[1] |= (value << 3) & 0xFF;
}


inline static uint8_t get_pressure(uint8_t *payload)
{
    /*
     * This function retrives the value pressure (9 bits).
     * The value spreads between bytes 1 and 2.
     * The value starts at bit 5.
     */

    return (payload[1] & 0x07) << 6 | payload[2] >> 2;
}

inline static void set_pressure(uint8_t *payload, uint8_t value)
{
    /*
     * This function writes the value pressure (9 bits).
     * The value will spread between bytes 1 and 2.
     * The value starts at bit 5.
     */

    payload[1] |= (value >> 6) & 0x07;
    payload[2] |= (value << 2) & 0xFF;
}


inline static uint8_t get_vbat(uint8_t *payload)
{
    /*
     * This function retrives the value vbat (7 bits).
     * The value spreads between bytes 2 and 3.
     * The value starts at bit 6.
     */

    return (payload[2] & 0x03) << 5 | payload[3] >> 3;
}

inline static void set_vbat(uint8_t *payload, uint8_t value)
{
    /*
     * This function writes the value vbat (7 bits).
     * The value will spread between bytes 2 and 3.
     * The value starts at bit 6.
     */

    payload[2] |= (value >> 5) & 0x03;
    payload[3] |= (value << 3) & 0xFF;
}


inline static uint32_t get_event_datetime(uint8_t *payload)
{
    /*
     * This function retrives the value event_datetime (32 bits).
     * The value spreads between bytes 3 and 7.
     * The value starts at bit 5.
     */

    return (payload[3] & 0x07) << 29 | payload[4] << 21 | payload[5] << 13 | payload[6] << 5 | payload[7] >> 3;
}

inline static void set_event_datetime(uint8_t *payload, uint32_t value)
{
    /*
     * This function writes the value event_datetime (32 bits).
     * The value will spread between bytes 3 and 7.
     * The value starts at bit 5.
     */

    payload[3] |= (value >> 29) & 0x07;
    payload[4] = (value >> 21) & 0xFF;
    payload[5] = (value >> 13) & 0xFF;
    payload[6] = (value >> 5) & 0xFF;
    payload[7] |= (value << 3) & 0xFF;
}


struct Payload
{

    uint32_t event_datetime;
    uint16_t mode;
    uint8_t version;
    uint8_t pressure;
    uint8_t vbat;
};



inline static void decode(struct Payload *payload, uint8_t *packed)
{
    /*
     * 
     */

    payload->version = get_version(packed);
    payload->mode = get_mode(packed);
    payload->pressure = get_pressure(packed);
    payload->vbat = get_vbat(packed);
    payload->event_datetime = get_event_datetime(packed);
}

inline static void encode(struct Payload *payload, uint8_t *packed)
{
    /*
     * 
     */

    set_version(packed, payload->version);
    set_mode(packed, payload->mode);
    set_pressure(packed, payload->pressure);
    set_vbat(packed, payload->vbat);
    set_event_datetime(packed, payload->event_datetime);
}

#endif //DEFAULT_MUNUD_HEADER_GUARD_H

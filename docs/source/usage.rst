
Usage
=====

Programmation API
-----------------

With munud, using bit-unaligned values can be done as such:

.. code-block:: python

    from munud import UnalignedBytes

    ub_A = UnalignedBytes(offset=0, bit_size=3)
    ub_B = UnalignedBytes(3, 17)
    ub_C = UnalignedBytes(20, 4)

    # Reading
    A = ub_A.extract(some_bytes)
    B = ub_B.extract(some_bytes)
    C = ub_C.extract(some_bytes)

    # Writing
    some_bytes = [0, 0, 0]

    ub_A.shove(byte_buffer=some_bytes, value=3)
    ub_B.shove(some_bytes, 1234)
    ub_C.shove(some_bytes, 5)

    # > b"\x60\x4d\x25"


C generation with the cli
-------------------------

Using the following `test.yml` file:

.. code-block:: yaml

    - name: A
    size: 3
    type: uint8_t
    - name: B
    size: 17
    type: uint16_t
    - name: C
    size: 4
    type: uint8_t


To show a table summarizing the payload, use :

.. code-block:: bash

    munud -f test.yml show


.. code-block:: none


                        Payload
    ┏━━━━━━━━━━┳━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┓
    ┃     Type ┃ Name ┃ Size (bits) ┃    Byte Span ┃
    ┡━━━━━━━━━━╇━━━━━━╇━━━━━━━━━━━━━╇━━━━━━━━━━━━━━┩
    │  uint8_t │    A │ 3           │ [0 (+0) - 1] │
    │ uint16_t │    B │ 17          │ [0 (+3) - 3] │
    │  uint8_t │    C │ 4           │ [2 (+4) - 3] │
    └──────────┴──────┴─────────────┴──────────────┘
    Total payload size: 24 bits


To decode an hex payload, use:

.. code-block:: bash

    munud -f test.yml decode -p AAAAAAAA


.. code-block:: none

            Payload
    ┏━━━━━━━━━━┳━━━━━━┳━━━━━━━┓
    ┃     Type ┃ Name ┃ Value ┃
    ┡━━━━━━━━━━╇━━━━━━╇━━━━━━━┩
    │  uint8_t │    A │     5 │
    │ uint16_t │    B │ 43690 │
    │  uint8_t │    C │    10 │
    └──────────┴──────┴───────┘
    Total payload size: 24 bits



To generate c getters and setters, use:

.. code-block:: bash

    munud -f test.yml cgen 



This will generate the following c code:

.. code-block:: c                                                                                                                                                                               
                                                                                                                                                                                            
    #include "assert.h"
    #include "stdint.h"
            

    inline static uint8_t get_A(uint8_t *payload)
    {
        /*
        * This function retrives the value A (3 bits).
        * The value is found in byte 0.
        * The value starts at bit 0.
        */

        return payload[0] >> 5;
    }

    inline static void set_A(uint8_t *payload, uint8_t value)
    {
        /*
        * This function writes the value A (3 bits).
        * The value will be written in byte 0.
        * The value starts at bit 0.
        */

        payload[0] |= value << 5;
    }


    inline static uint16_t get_B(uint8_t *payload)
    {
        /*
        * This function retrives the value B (17 bits).
        * The value spreads between bytes 0 and 2.
        * The value starts at bit 3.
        */

        return (payload[0] & 0x1F) << 12 | payload[1] << 4 | payload[2] >> 4;
    }

    inline static void set_B(uint8_t *payload, uint16_t value)
    {
        /*
        * This function writes the value B (17 bits).
        * The value will spread between bytes 0 and 2.
        * The value starts at bit 3.
        */

        payload[0] |= (value >> 12) & 0x1F;
        payload[1] = (value >> 4) & 0xFF;
        payload[2] |= (value << 4) & 0xFF;
    }


    inline static uint8_t get_C(uint8_t *payload)
    {
        /*
        * This function retrives the value C (4 bits).
        * The value is found in byte 2.
        * The value starts at bit 4.
        */

        return payload[2] & 0x0F;
    }

    inline static void set_C(uint8_t *payload, uint8_t value)
    {
        /*
        * This function writes the value C (4 bits).
        * The value will be written in byte 20.
        * The value starts at bit 4.
        */

        payload[2] |= value;
    }


    struct Payload
    {

        uint16_t B;
        uint8_t A;
        uint8_t C;
    };



    inline static void decode(struct Payload *payload, uint8_t *packed)
    {
        /*
        * 
        */

        payload->A = get_A(packed);
        payload->B = get_B(packed);
        payload->C = get_C(packed);
    }

    inline static void encode(struct Payload *payload, uint8_t *packed)
    {
        /*
        * 
        */

        set_A(packed, payload->A);
        set_B(packed, payload->B);
        set_C(packed, payload->C);
    }


Many options are available for customisation :

.. code-block:: bash

    $ munud cgen --help

    Usage: munud cgen [OPTIONS]

    Options:
    -f, --fmt TEXT          A file describing the wanted format.
    -o, --output TEXT       The output .h file.
    --crlf                  Use \r\n (crlf) as newline instead of \n (crlf).
    --use-assert            Generate assert check before writing to payload.
    --get-only              Generate only getters.
    --set-only              Generate only setters.
    --without-struct        Do not generate a struct containing the payload.
    --packed-struct         Add the gcc __attribute__((packed)) attribute to the
                            struct.
    -n, --struct-name TEXT  Payload struct name.
    --without-safety-mask   Removes the 0xff mask when writing ints on bytes.
    --payload-type TEXT     The type of the payload, usually uint8_t*.
    --cpp                   Generate cpp-style namespace.
    --namespace TEXT        The name of an optional namespace. If cpp is not
                            enabled, add it as a prefix.
    --help                  Show this message and exit.
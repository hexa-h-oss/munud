munud API
=========

.. .. autoenum:: src.stb_kineis::FixType
..    :members:

.. .. autoenum:: src.stb_kineis::Mode
..    :members:

.. .. autoenum:: src.stb_kineis::Event
..    :members:

.. .. autoclass:: src.stb_kineis::TriggerEvents
..    :undoc-members:
..    :members:

.. .. autoclass:: src.stb_kineis::SwitchStates
..    :undoc-members:
..    :members:

.. .. autoclass:: src.stb_kineis::KineisPayload
..    :undoc-members:
..    :members:

.. .. autoclass:: src.stb_kineis::KineisDecoder
..    :undoc-members:
..    :members:
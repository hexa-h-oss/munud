Introduction
============

Building payloads with sub-byte unaligned values, and generating according C code.

Installation
------------

The easiest way to install this library is using pip:

.. code-block:: bash

    pip install munud


Motivation
----------

This module is built around the concept of "Unaligned Bytes", which means a sequence
of n bits forming an int which might not be aligned on the bytes themselves.

Take as an example the following bytes : 

.. code-block:: python

    some_bytes = b"\xfa\x04\xde"


We will assume that the first 3 bits corresponds to value A, the next 17 bits to value B, and
the next 4 bits to value C. This will correspond to the following representation in memory:

.. code-block:: none

    +--------+--------+--------+
    | Byte 0 | Byte 1 | Byte 2 |
    +---+----+--------+---+----+
    | A |        B        | C  |
    +---+-----------------+----+


Thus, a traditionnal python way of retrieving those three values would be as such:

.. code-block:: python

    A = some_bytes[0] >> 5
    B = (payload[0] & 0x1F) << 12 | payload[1] << 4 | payload[2] >> 4
    C = some_bytes[2] & 0x0F

And a traditional way of shoving those values in a byte array would be as follows:

.. code-block:: python

    some_bytes = [0, 0, 0]

    some_bytes[0] |= A << 5

    some_bytes[0] |= (B >> 12) & 0x1F
    some_bytes[1] = (B >> 4) & 0xFF
    some_bytes[2] |= (B << 4) & 0xFF

    some_bytes[2] |= C


This module provides two main functionalities:

- Provide a python API to do those read/write operations dynamically
- Generate a C code to perform those operations efficiently, given a fixed format